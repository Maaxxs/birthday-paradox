"""The probability that at least two persons have their birthday on the same day"""

def _partial_fak(persons):
    result = 365
    for i in range(1, persons):
        result *= (365 - i)

    return result


def probability(persons):
    """Return the probability that two people don't have their
        birthday on the same day"""
    return (1 - (_partial_fak(persons) / (365 ** persons))) * 100



print('The probability that at least two persons have their birthday on the same day')
print()

for i in range(1, 51):
    print(f'{i:02} People: ', end='')
    print(f'{probability(i):05.2f} %')

